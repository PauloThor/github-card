import { useState } from "react";
import "./App.css";
import Form from "./components/Form";
import { Ul } from "./components/StyledComponents";
import { Col, Container } from "reactstrap";

function App() {
  const [result, setResult] = useState([]);
  const [owner, setOwner] = useState("");

  return (
    <div className="App">
      <Form
        result={result}
        setResult={setResult}
        owner={owner}
        setOwner={setOwner}
      />
      <Ul>
        {result.map((project, i) => (
          <li key={i}>
            <Container style={{ display: "flex" }}>
              <Col>
                <img src={project.image} alt={project.name} />
              </Col>
              <Col>
                <h3>{project.name}</h3>
                <p>{project.description}</p>
              </Col>
            </Container>
          </li>
        ))}
      </Ul>
    </div>
  );
}

export default App;
