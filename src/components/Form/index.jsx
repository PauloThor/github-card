import { useState } from "react";
import { Button, Input } from "../StyledComponents";

import "./styles.css";

const Form = ({ result, setResult, owner, setOwner }) => {
  const [repo, setRepo] = useState("");
  const [error, setError] = useState("");

  const submitRepo = (e) => {
    e.preventDefault();

    if (owner.length === 0 || repo.length === 0) {
      setError("Digite nos campos");
      return;
    }

    fetch(`https://api.github.com/repos/${owner}/${repo}`)
      .then((response) => response.json())
      .then((response) =>
        setResult([
          ...result,
          {
            name: response.name,
            image: response.owner.avatar_url,
            description: response.description,
          },
        ])
      )
      .catch((res) => {
        setError("Repositório não encontrado");
      });
  };

  return (
    <form onSubmit={submitRepo}>
      <Input
        onChange={(e) => {
          setOwner(e.target.value);
        }}
        placeholder="Digite o nome do dono"
      />
      <Input
        onChange={(e) => setRepo(e.target.value)}
        placeholder="Digite o nome do repositório"
      />
      <Button onClick={submitRepo}>Pesquisar</Button>
      <p>{error.length > 0 && error}</p>
    </form>
  );
};

export default Form;
