import styled from "styled-components";

export const Input = styled.input`
  /* border-radius: 10px; */
  padding: 0.5rem;
  height: 30px;
`;

export const Button = styled.button`
  width: 200px;
  height: 50px;
  background-color: lightblue;
`;

export const Ul = styled.ul`
  list-style: none;
  li {
    background-color: gray;
    padding: 0.2rem;
    margin: 1rem;
    width: 700px;
    margin: 1rem auto;
    border-radius: 10px;

    h3 {
      text-align: start;
    }
  }
  img {
    width: 70px;
    padding: 1rem;
  }
`;
